Server za bazu je u folderu MongoDB. Njega iskopirate negde na file system. Da bi pokrenuli server, u cmd udjete u path:
MongoDB\Server\3.2\bin i onda ukucate komdandu: mongod --dbpath <path>, a za path odaberete gde vam je db folder.

Folder database je baza sa kojom smo mi radili dok smo pravili app, tako da mozete nju da koristite najbolje ( njoj vam je taj folde db).