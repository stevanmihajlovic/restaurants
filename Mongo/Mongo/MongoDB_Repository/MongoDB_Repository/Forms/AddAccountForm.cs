﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MongoDB_Repository.Domain_models;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;

namespace MongoDB_Repository.Forms
{
    public partial class AddAccountForm : Form
    {
        MongoServer server;
        HomeForm home;

        public AddAccountForm(MongoServer s,HomeForm form)
        {
            server = s;
            home = form;
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!(nameTB.Text.Equals("") || lastnameTB.Text.Equals("") || usernameTB.Text.Equals("") || passwordTB.Text.Equals("")))
            {
                User user = new User();
                user.Name = nameTB.Text;
                user.Lastname = lastnameTB.Text;
                user.Username = usernameTB.Text;
                user.Password = passwordTB.Text;

                MongoDatabase db = server.GetDatabase("RecipeDatabase");
                MongoCollection collection = db.GetCollection("users");

                IMongoQuery query = Query.EQ("Username", user.Username);
                MongoCursor<User> users = collection.FindAs<User>(query);
                User[] list = users.ToArray<User>();
                if (list.Length == 0)//check if unique
                {
                    collection.Insert(user);
                    home.user = user;
                    MessageBox.Show("Succesfully added " + user.Username + " as a new user!");
                    Dispose();
                }
                else
                {
                    MessageBox.Show("Username already exists!");
                }
            }
            else
                MessageBox.Show("Please fill all fields!");
        }
    }
}
