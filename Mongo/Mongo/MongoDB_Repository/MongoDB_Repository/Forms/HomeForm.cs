﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB_Repository.Domain_models;
using MongoDB.Driver.Builders;

namespace MongoDB_Repository.Forms
{
    public partial class HomeForm : Form
    {
        MongoServer server;
        public User user;
        List<Recipe> suggestedReceipts;
        public HomeForm()
        {
            server = MongoServer.Create("mongodb://localhost/?safe=true");
            user = null;
            suggestedReceipts = new List<Recipe>();
            InitializeComponent();
        }

        private void HomeForm_Load(object sender, EventArgs e)
        {
            GetSuggReceipts();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (user != null)
            {
                AddRecipe form = new AddRecipe(server,user,true);
                form.ShowDialog();
            }
            else
            {
                MessageBox.Show("You have to be logged in to write receipt.");

            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            LogInForm lform = new LogInForm(server, this);
            lform.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            SearchReceiptForm form = new SearchReceiptForm(server,user);
            form.ShowDialog();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            PictureBox pb = new PictureBox();
            pb.SizeMode = PictureBoxSizeMode.StretchImage;
            pb.Width = 80;
            pb.Height = 80;
            pb.Load("mario.jpg");
            flp.Controls.Add(pb);
            pb.DoubleClick += new EventHandler(funkcija);

        }


        private void funkcija (object sender, System.EventArgs e)
        {
            Control box = (Control)sender;
            int index = flp.Controls.IndexOf(box);
            Recipe r = suggestedReceipts[index];

            AddRecipe ad = new AddRecipe(server, user, false);  //ovde se vec odluci izgled forme
            ad.LoadRecipe(r);
            ad.ShowDialog();

        }
        private void GetSuggReceipts ()
        {
            MongoDatabase db = server.GetDatabase("RecipeDatabase");
            MongoCollection collection = db.GetCollection ("recipes");
            MongoCursor<Recipe> receipts = collection.FindAllAs<Recipe>().SetLimit(8);
            suggestedReceipts = receipts.ToList<Recipe>();
            foreach (Recipe r in suggestedReceipts)
            {
                PictureBox pb = new PictureBox();
                pb.SizeMode = PictureBoxSizeMode.StretchImage;
                pb.Width = 120;
                pb.Height = 120;
                pb.Image = r.image;
                flp.Controls.Add(pb);
                pb.DoubleClick += new EventHandler(funkcija);
            }
        }
    }
}
