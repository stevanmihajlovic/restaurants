﻿namespace MongoDB_Repository.Forms
{
    partial class AddRecipe
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.nameTB = new System.Windows.Forms.TextBox();
            this.labelName = new System.Windows.Forms.Label();
            this.labelIngredients = new System.Windows.Forms.Label();
            this.labelRecipe = new System.Windows.Forms.Label();
            this.recipeTB = new System.Windows.Forms.RichTextBox();
            this.buttonAddRecipe = new System.Windows.Forms.Button();
            this.ingredientsLV = new System.Windows.Forms.ListView();
            this.labelIngredientName = new System.Windows.Forms.Label();
            this.labelQuantity = new System.Windows.Forms.Label();
            this.ingrQuantityTB = new System.Windows.Forms.TextBox();
            this.buttonAddIngridient = new System.Windows.Forms.Button();
            this.labelTimeToCook = new System.Windows.Forms.Label();
            this.timrTB = new System.Windows.Forms.TextBox();
            this.labelType = new System.Windows.Forms.Label();
            this.typeCB = new System.Windows.Forms.ComboBox();
            this.labelDirections = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.buttonAddImage = new System.Windows.Forms.Button();
            this.labelMinutes = new System.Windows.Forms.Label();
            this.buttonRemove = new System.Windows.Forms.Button();
            this.buttonCreateNewIngredient = new System.Windows.Forms.Button();
            this.comboBoxIngredients = new System.Windows.Forms.ComboBox();
            this.comboBoxAmount = new System.Windows.Forms.ComboBox();
            this.labelAuthor = new System.Windows.Forms.Label();
            this.buttonModifyRecipe = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // nameTB
            // 
            this.nameTB.Location = new System.Drawing.Point(92, 32);
            this.nameTB.Name = "nameTB";
            this.nameTB.Size = new System.Drawing.Size(180, 20);
            this.nameTB.TabIndex = 0;
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Location = new System.Drawing.Point(12, 32);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(35, 13);
            this.labelName.TabIndex = 1;
            this.labelName.Text = "Name";
            // 
            // labelIngredients
            // 
            this.labelIngredients.AutoSize = true;
            this.labelIngredients.Location = new System.Drawing.Point(12, 126);
            this.labelIngredients.Name = "labelIngredients";
            this.labelIngredients.Size = new System.Drawing.Size(59, 13);
            this.labelIngredients.TabIndex = 2;
            this.labelIngredients.Text = "Ingredients";
            // 
            // labelRecipe
            // 
            this.labelRecipe.AutoSize = true;
            this.labelRecipe.Location = new System.Drawing.Point(89, 9);
            this.labelRecipe.Name = "labelRecipe";
            this.labelRecipe.Size = new System.Drawing.Size(44, 13);
            this.labelRecipe.TabIndex = 3;
            this.labelRecipe.Text = "Recipe:";
            // 
            // recipeTB
            // 
            this.recipeTB.Location = new System.Drawing.Point(306, 256);
            this.recipeTB.Name = "recipeTB";
            this.recipeTB.Size = new System.Drawing.Size(281, 96);
            this.recipeTB.TabIndex = 4;
            this.recipeTB.Text = "";
            // 
            // buttonAddRecipe
            // 
            this.buttonAddRecipe.Location = new System.Drawing.Point(490, 369);
            this.buttonAddRecipe.Name = "buttonAddRecipe";
            this.buttonAddRecipe.Size = new System.Drawing.Size(102, 23);
            this.buttonAddRecipe.TabIndex = 5;
            this.buttonAddRecipe.Text = "Add recipe";
            this.buttonAddRecipe.UseVisualStyleBackColor = true;
            this.buttonAddRecipe.Click += new System.EventHandler(this.button1_Click);
            // 
            // ingredientsLV
            // 
            this.ingredientsLV.Location = new System.Drawing.Point(92, 126);
            this.ingredientsLV.MultiSelect = false;
            this.ingredientsLV.Name = "ingredientsLV";
            this.ingredientsLV.Size = new System.Drawing.Size(180, 108);
            this.ingredientsLV.TabIndex = 6;
            this.ingredientsLV.UseCompatibleStateImageBehavior = false;
            // 
            // labelIngredientName
            // 
            this.labelIngredientName.AutoSize = true;
            this.labelIngredientName.Location = new System.Drawing.Point(89, 283);
            this.labelIngredientName.Name = "labelIngredientName";
            this.labelIngredientName.Size = new System.Drawing.Size(86, 13);
            this.labelIngredientName.TabIndex = 8;
            this.labelIngredientName.Text = "Ingredient name:";
            // 
            // labelQuantity
            // 
            this.labelQuantity.AutoSize = true;
            this.labelQuantity.Location = new System.Drawing.Point(89, 325);
            this.labelQuantity.Name = "labelQuantity";
            this.labelQuantity.Size = new System.Drawing.Size(49, 13);
            this.labelQuantity.TabIndex = 10;
            this.labelQuantity.Text = "Quantity:";
            // 
            // ingrQuantityTB
            // 
            this.ingrQuantityTB.Location = new System.Drawing.Point(92, 342);
            this.ingrQuantityTB.Name = "ingrQuantityTB";
            this.ingrQuantityTB.Size = new System.Drawing.Size(100, 20);
            this.ingrQuantityTB.TabIndex = 11;
            // 
            // buttonAddIngridient
            // 
            this.buttonAddIngridient.Location = new System.Drawing.Point(172, 369);
            this.buttonAddIngridient.Name = "buttonAddIngridient";
            this.buttonAddIngridient.Size = new System.Drawing.Size(100, 23);
            this.buttonAddIngridient.TabIndex = 12;
            this.buttonAddIngridient.Text = "Add Ingridient";
            this.buttonAddIngridient.UseVisualStyleBackColor = true;
            this.buttonAddIngridient.Click += new System.EventHandler(this.button2_Click);
            // 
            // labelTimeToCook
            // 
            this.labelTimeToCook.AutoSize = true;
            this.labelTimeToCook.Location = new System.Drawing.Point(12, 100);
            this.labelTimeToCook.Name = "labelTimeToCook";
            this.labelTimeToCook.Size = new System.Drawing.Size(72, 13);
            this.labelTimeToCook.TabIndex = 13;
            this.labelTimeToCook.Text = "Time to cook:";
            // 
            // timrTB
            // 
            this.timrTB.Location = new System.Drawing.Point(92, 100);
            this.timrTB.Name = "timrTB";
            this.timrTB.Size = new System.Drawing.Size(63, 20);
            this.timrTB.TabIndex = 14;
            // 
            // labelType
            // 
            this.labelType.AutoSize = true;
            this.labelType.Location = new System.Drawing.Point(12, 65);
            this.labelType.Name = "labelType";
            this.labelType.Size = new System.Drawing.Size(31, 13);
            this.labelType.TabIndex = 16;
            this.labelType.Text = "Type";
            // 
            // typeCB
            // 
            this.typeCB.FormattingEnabled = true;
            this.typeCB.Items.AddRange(new object[] {
            "Aperitif",
            "Breakfast",
            "Main course",
            "Appetizer",
            "Dessert",
            "Base",
            "Drinks",
            "Side dish",
            "Other"});
            this.typeCB.Location = new System.Drawing.Point(92, 65);
            this.typeCB.Name = "typeCB";
            this.typeCB.Size = new System.Drawing.Size(121, 21);
            this.typeCB.TabIndex = 17;
            // 
            // labelDirections
            // 
            this.labelDirections.AutoSize = true;
            this.labelDirections.Location = new System.Drawing.Point(303, 240);
            this.labelDirections.Name = "labelDirections";
            this.labelDirections.Size = new System.Drawing.Size(57, 13);
            this.labelDirections.TabIndex = 18;
            this.labelDirections.Text = "Directions:";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(306, 32);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(281, 146);
            this.pictureBox1.TabIndex = 19;
            this.pictureBox1.TabStop = false;
            // 
            // buttonAddImage
            // 
            this.buttonAddImage.Location = new System.Drawing.Point(490, 184);
            this.buttonAddImage.Name = "buttonAddImage";
            this.buttonAddImage.Size = new System.Drawing.Size(102, 23);
            this.buttonAddImage.TabIndex = 20;
            this.buttonAddImage.Text = "Add Image";
            this.buttonAddImage.UseVisualStyleBackColor = true;
            this.buttonAddImage.Click += new System.EventHandler(this.button3_Click);
            // 
            // labelMinutes
            // 
            this.labelMinutes.AutoSize = true;
            this.labelMinutes.Location = new System.Drawing.Point(162, 106);
            this.labelMinutes.Name = "labelMinutes";
            this.labelMinutes.Size = new System.Drawing.Size(57, 13);
            this.labelMinutes.TabIndex = 21;
            this.labelMinutes.Text = "in minutes.";
            // 
            // buttonRemove
            // 
            this.buttonRemove.Location = new System.Drawing.Point(197, 254);
            this.buttonRemove.Name = "buttonRemove";
            this.buttonRemove.Size = new System.Drawing.Size(75, 23);
            this.buttonRemove.TabIndex = 22;
            this.buttonRemove.Text = "Remove ";
            this.buttonRemove.UseVisualStyleBackColor = true;
            this.buttonRemove.Click += new System.EventHandler(this.removeButton_Click);
            // 
            // buttonCreateNewIngredient
            // 
            this.buttonCreateNewIngredient.Location = new System.Drawing.Point(172, 398);
            this.buttonCreateNewIngredient.Name = "buttonCreateNewIngredient";
            this.buttonCreateNewIngredient.Size = new System.Drawing.Size(100, 38);
            this.buttonCreateNewIngredient.TabIndex = 23;
            this.buttonCreateNewIngredient.Text = "Create New Ingredient";
            this.buttonCreateNewIngredient.UseVisualStyleBackColor = true;
            this.buttonCreateNewIngredient.Click += new System.EventHandler(this.buttonCreateNewIngredient_Click);
            // 
            // comboBoxIngredients
            // 
            this.comboBoxIngredients.FormattingEnabled = true;
            this.comboBoxIngredients.Location = new System.Drawing.Point(92, 299);
            this.comboBoxIngredients.Name = "comboBoxIngredients";
            this.comboBoxIngredients.Size = new System.Drawing.Size(100, 21);
            this.comboBoxIngredients.TabIndex = 24;
            // 
            // comboBoxAmount
            // 
            this.comboBoxAmount.FormattingEnabled = true;
            this.comboBoxAmount.Items.AddRange(new object[] {
            "ml",
            "cl",
            "dl",
            "l",
            "g",
            "kg",
            "pcs"});
            this.comboBoxAmount.Location = new System.Drawing.Point(225, 342);
            this.comboBoxAmount.Name = "comboBoxAmount";
            this.comboBoxAmount.Size = new System.Drawing.Size(47, 21);
            this.comboBoxAmount.TabIndex = 25;
            // 
            // labelAuthor
            // 
            this.labelAuthor.AutoSize = true;
            this.labelAuthor.Location = new System.Drawing.Point(303, 221);
            this.labelAuthor.Name = "labelAuthor";
            this.labelAuthor.Size = new System.Drawing.Size(0, 13);
            this.labelAuthor.TabIndex = 26;
            // 
            // buttonModifyRecipe
            // 
            this.buttonModifyRecipe.Location = new System.Drawing.Point(490, 369);
            this.buttonModifyRecipe.Name = "buttonModifyRecipe";
            this.buttonModifyRecipe.Size = new System.Drawing.Size(102, 23);
            this.buttonModifyRecipe.TabIndex = 27;
            this.buttonModifyRecipe.Text = "Modify Recipe";
            this.buttonModifyRecipe.UseVisualStyleBackColor = true;
            this.buttonModifyRecipe.Visible = false;
            this.buttonModifyRecipe.Click += new System.EventHandler(this.buttonModifyRecipe_Click);
            // 
            // AddRecipe
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(604, 457);
            this.Controls.Add(this.buttonModifyRecipe);
            this.Controls.Add(this.labelAuthor);
            this.Controls.Add(this.comboBoxAmount);
            this.Controls.Add(this.comboBoxIngredients);
            this.Controls.Add(this.buttonCreateNewIngredient);
            this.Controls.Add(this.buttonRemove);
            this.Controls.Add(this.labelMinutes);
            this.Controls.Add(this.buttonAddImage);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.labelDirections);
            this.Controls.Add(this.typeCB);
            this.Controls.Add(this.labelType);
            this.Controls.Add(this.timrTB);
            this.Controls.Add(this.labelTimeToCook);
            this.Controls.Add(this.buttonAddIngridient);
            this.Controls.Add(this.ingrQuantityTB);
            this.Controls.Add(this.labelQuantity);
            this.Controls.Add(this.labelIngredientName);
            this.Controls.Add(this.ingredientsLV);
            this.Controls.Add(this.buttonAddRecipe);
            this.Controls.Add(this.recipeTB);
            this.Controls.Add(this.labelRecipe);
            this.Controls.Add(this.labelIngredients);
            this.Controls.Add(this.labelName);
            this.Controls.Add(this.nameTB);
            this.Name = "AddRecipe";
            this.Text = "AddReceipt";
            this.Load += new System.EventHandler(this.AddReceipt_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox nameTB;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.Label labelIngredients;
        private System.Windows.Forms.Label labelRecipe;
        private System.Windows.Forms.RichTextBox recipeTB;
        private System.Windows.Forms.Button buttonAddRecipe;
        private System.Windows.Forms.ListView ingredientsLV;
        private System.Windows.Forms.Label labelIngredientName;
        private System.Windows.Forms.Label labelQuantity;
        private System.Windows.Forms.TextBox ingrQuantityTB;
        private System.Windows.Forms.Button buttonAddIngridient;
        private System.Windows.Forms.Label labelTimeToCook;
        private System.Windows.Forms.TextBox timrTB;
        private System.Windows.Forms.Label labelType;
        private System.Windows.Forms.ComboBox typeCB;
        private System.Windows.Forms.Label labelDirections;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button buttonAddImage;
        private System.Windows.Forms.Label labelMinutes;
        private System.Windows.Forms.Button buttonRemove;
        private System.Windows.Forms.Button buttonCreateNewIngredient;
        private System.Windows.Forms.ComboBox comboBoxIngredients;
        private System.Windows.Forms.ComboBox comboBoxAmount;
        private System.Windows.Forms.Label labelAuthor;
        private System.Windows.Forms.Button buttonModifyRecipe;
    }
}