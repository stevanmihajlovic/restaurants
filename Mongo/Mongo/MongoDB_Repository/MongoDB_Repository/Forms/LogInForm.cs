﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MongoDB_Repository.Domain_models;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;
using MongoDB_Repository.Forms;


namespace MongoDB_Repository.Forms
{
    public partial class LogInForm : Form
    {
        MongoServer server;
        HomeForm home;

        public LogInForm(MongoServer s, HomeForm form)
        {
            server = s;
            home = form;
            InitializeComponent();
        }

        private void logIn_Click(object sender, EventArgs e)
        {
            User user;
            IMongoQuery query = Query.And(Query.EQ("Username", usernameTB.Text), Query.EQ("Password", passwordTB.Text));
            MongoDatabase db = server.GetDatabase("RecipeDatabase");
            MongoCollection collection = db.GetCollection("users");
            MongoCursor<User> logUser = collection.FindAs<User>(query);
            User[] list = logUser.ToArray<User>();
            if (list.Length == 1)
            {
                home.user = list[0];
                this.Dispose();
            }
            else
            {
                MessageBox.Show("Wrong username or password!");
            }            

        }

        private void addaccButton_Click(object sender, EventArgs e)
        {
            AddAccountForm form = new AddAccountForm(server,home);
            form.ShowDialog();
            this.Dispose();
        }
    }
}
