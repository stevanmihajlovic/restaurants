﻿using MongoDB_Repository.Domain_models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MongoDB.Bson;
using MongoDB.Driver;

using System.IO;

namespace MongoDB_Repository.Forms
{
    public partial class AddRecipe : Form
    {
        MongoServer server;
        User user;
        bool addView;
        List<Ingredient> ingredients;
        List<Ingredient> addedIngredients;
        Recipe recipe;

        public AddRecipe(MongoServer s, User u, bool addView)
        {
            server = s;
            user = u;
            this.addView = addView;
            ingredients = new List<Ingredient>();
            addedIngredients = new List<Ingredient>();
            InitializeComponent();
            loadIngredients();
        }



        private void AddReceipt_Load(object sender, EventArgs e)
        {
            ingredientsLV.View = View.Details;
            ingredientsLV.GridLines = true;
            ingredientsLV.FullRowSelect = true;

            ingredientsLV.Columns.Add("Ingridient name", 100);
            ingredientsLV.Columns.Add("Amount", 100);

            //odavde prebacen load ingredients

            if (!addView)
            {
                buttonAddRecipe.Visible = false;
                buttonAddIngridient.Visible = false;
                buttonAddImage.Visible = false;
                buttonRemove.Visible = false;
                labelIngredientName.Visible = false;
                labelQuantity.Visible = false;
                comboBoxIngredients.Visible = false;
                comboBoxAmount.Visible = false;
                buttonCreateNewIngredient.Visible = false;
                ingrQuantityTB.Visible = false;
                typeCB.Enabled = false;
                timrTB.Enabled = false;
                recipeTB.Enabled = false;
            }

        }

        private void loadIngredients()
        {
            comboBoxIngredients.Items.Clear();
            MongoDatabase db = server.GetDatabase("RecipeDatabase");
            MongoCollection collection = db.GetCollection("ingredients");
            MongoCursor<Ingredient> ings = collection.FindAllAs<Ingredient>();
            ingredients = ings.ToList<Ingredient>();
            foreach (Ingredient i in ingredients)
                comboBoxIngredients.Items.Add(i.Name);
        }

        private void button2_Click(object sender, EventArgs e)//add ingredient
        {
            /*if (ingrNameTB.Text != "")
            {
                ListViewItem item;
                string[] values = new string[2];
                values[0] = ingrNameTB.Text;
                values[1] = ingrQuantityTB.Text;
                item = new ListViewItem(values);
                ingredientsLV.Items.Add(item);
                ingrNameTB.Text = "";
                ingrQuantityTB.Text = "";
            }*/

            if (comboBoxIngredients.SelectedIndex != -1 && comboBoxAmount.SelectedIndex != -1)
            {
                string[] values = new string[2];
                values[0] = comboBoxIngredients.SelectedItem.ToString();
                int quantity;
                if (int.TryParse(ingrQuantityTB.Text, out quantity))
                {
                    values[1] = quantity + " " + comboBoxAmount.SelectedItem.ToString();
                    ListViewItem item = new ListViewItem(values);
                    ingredientsLV.Items.Add(item);
                    ingrQuantityTB.Text = "";
                    comboBoxAmount.SelectedIndex = -1;
                    addedIngredients.Add(ingredients[comboBoxIngredients.SelectedIndex]);
                    ingredients.RemoveAt(comboBoxIngredients.SelectedIndex);
                    comboBoxIngredients.Items.RemoveAt(comboBoxIngredients.SelectedIndex);
                }
                else
                    MessageBox.Show("Please input only numbers for quantity!");
            }
            else
                MessageBox.Show("Please select ingredient first and its quantity!");
        }

        private void button1_Click(object sender, EventArgs e)//add recipe
        {
            if (ingredientsLV.Items.Count!=0 && nameTB.Text != "" && recipeTB.Text != "" && timrTB.Text != "")
            {
                try
                {
                    Recipe recipe = new Recipe();
                    recipe.owner = new MongoDBRef("users", user.id);
                    //cudno je bilo ovde
                    recipe.Name = nameTB.Text;
                    recipe.Type = typeCB.SelectedItem.ToString();
                    recipe.Directions = recipeTB.Text;
                    recipe.TimeToPrepare = Int16.Parse(timrTB.Text);
                    recipe.image = pictureBox1.Image;

                    int i = 0;
                    foreach (ListViewItem item in ingredientsLV.Items)
                    {
                        recipe.ingredients.Add(new MongoDBRef("ingredients", addedIngredients[i++].id));
                        recipe.quantities.Add(item.SubItems[1].Text);
                    }

                    recipe.AddRecipe(server);

                    user.Receipts.Add(new MongoDBRef("recipes", recipe.id));//OVO JE CUDNO!

                    MessageBox.Show("Thank you for posting your recipe.");
                    this.Dispose();

                }
                catch (FileFormatException err)
                {
                    MessageBox.Show("Please choose picture with lower quality!");
                }
                catch (Exception err)
                {
                    MessageBox.Show(err.Message);
                }
            }
            else
            {
                MessageBox.Show("Please fill all fields!");
            }
        }

        private void button3_Click(object sender, EventArgs e)//add picture
        {
            try
            {
                OpenFileDialog fd = new OpenFileDialog();
                fd.Multiselect = false;
                if (fd.ShowDialog() == DialogResult.OK)
                {
                    pictureBox1.Load(fd.FileName);
                    pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
                }
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
            }
        }
        public void LoadRecipe (Recipe r)
        {
            recipe = r;
            nameTB.Text = r.Name;
            typeCB.SelectedText = r.Type;
            timrTB.Text = r.TimeToPrepare.ToString ();
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox1.Image = r.image;
            recipeTB.Text = r.Directions;
            findAuthor(r);
            MongoDatabase db = server.GetDatabase("RecipeDatabase");
            int i = 0;
            foreach (MongoDBRef ing in r.ingredients)
            {
                string[] values = new string[2];
                Ingredient ingredient = db.FetchDBRefAs<Ingredient>(ing);
                values[0] = ingredient.Name;
                values[1] = r.quantities[i++];
                ListViewItem item = new ListViewItem(values);
                ingredientsLV.Items.Add(item);

                addedIngredients.Add(ingredient);
                List<string> names = ingredients.Select(x => x.Name).ToList();
                comboBoxIngredients.Items.RemoveAt(names.IndexOf(ingredient.Name));    //OVDE NE NALAZI PO REFERENCI LEPO INDEX
                ingredients.RemoveAt(names.IndexOf(ingredient.Name));
            }
            
        }

        private void findAuthor(Recipe r)
        {
            MongoDatabase db = server.GetDatabase("RecipeDatabase");
            //MongoCollection collection = db.GetCollection("ingredients");
            User u = db.FetchDBRefAs<User>(r.owner);
            labelAuthor.Text = "Author : " + u.Username;
            if (user != null && u.Username.Equals(user.Username))
            {
                addView = true;
                //buttonAddRecipe.Visible = false;
                buttonModifyRecipe.Visible = true;
                //modify
            }
        }

        private void removeButton_Click(object sender, EventArgs e)
        {
            if (addedIngredients.Count == 0)
                MessageBox.Show("Please add ingredients before trying to remove them!");
            else
                if (ingredientsLV.SelectedItems.Count != 0)                    
                {
                    ingredients.Add(addedIngredients[ingredientsLV.SelectedIndices[0]]);    //o quantitijima ne mora da brinemo
                    comboBoxIngredients.Items.Add(ingredients.Last().Name);
                    addedIngredients.RemoveAt(ingredientsLV.SelectedIndices[0]);
                    ingredientsLV.Items.Remove(ingredientsLV.SelectedItems[0]);
                }
                else
                    MessageBox.Show("Please select which ingredient you wish to remove first!");
            
        }

        private void buttonCreateNewIngredient_Click(object sender, EventArgs e)
        {
            CreateNewIngredient form = new CreateNewIngredient(server, ingredients);
            form.ShowDialog();

            if (ingredients.Count>0 && !comboBoxIngredients.Items.Contains(ingredients.Last().Name))
                comboBoxIngredients.Items.Add(ingredients.Last().Name);
        }

        private void buttonModifyRecipe_Click(object sender, EventArgs e)
        {
            button1_Click(sender, e);   //napravimo na isti nacin pa da izbrisemo stari ako uspe?
            //to do, update query
        }
    }
}
