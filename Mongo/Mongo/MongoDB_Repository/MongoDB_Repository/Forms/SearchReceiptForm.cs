﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB_Repository.Domain_models;
using MongoDB.Driver.Builders;
using MongoDB_Repository.Forms;

namespace MongoDB_Repository.Forms
{
    public partial class SearchReceiptForm : Form
    {
        MongoServer server;
        User user;
        List<Recipe> foundRecipes;

        public SearchReceiptForm(MongoServer s, User u)
        {
            server = s;
            user = u;
            foundRecipes = new List<Recipe>();
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            listView1.Items.Clear();
            foundRecipes.Clear();
            IMongoQuery query = Query.Null, nameQuery = Query.Null,typeQuery = Query.Null,timeQuery = Query.Null;
            

            if (nameTB.Text != "")
            {               
                nameQuery = Query.EQ("Name", nameTB.Text);
                query = nameQuery;
            }

            if (comboBox1.SelectedItem != null)
            {
                if (comboBox1.SelectedItem.ToString() != "")
                {
                    typeQuery = Query.EQ("Type", comboBox1.SelectedItem.ToString());
                    query = (query != Query.Null) ? Query.And(query, typeQuery) : typeQuery;
                }
            }
            if (timeTB.Text != "")
            {
                int time = Int16.Parse(timeTB.Text);
                timeQuery = Query.LTE("TimeToPrepare", time);
                query = (query != Query.Null) ? Query.And (query,timeQuery) : timeQuery;
            }          

            MongoDatabase db = server.GetDatabase("RecipeDatabase");
            MongoCollection collection = db.GetCollection("recipes");
            MongoCursor<Recipe> receipt = collection.FindAs<Recipe>(query);
            foundRecipes = receipt.ToList<Recipe>();
            

            foreach (Recipe r in foundRecipes)
            {
                ListViewItem item = new ListViewItem(new string[]{r.Name, r.Type,r.TimeToPrepare.ToString ()});
                listView1.Items.Add(item);
            }

            if (foundRecipes.Count == 0)
                MessageBox.Show("No recipes found!");

        }

        private void SearchReceiptForm_Load(object sender, EventArgs e)
        {
            listView1.FullRowSelect = true;
            listView1.View = View.Details;
            listView1.GridLines = true;
            

            listView1.Columns.Add("Recipe:",90);
            listView1.Columns.Add("Type:",80);
            listView1.Columns.Add("Time to prepare:", 50);


        }

        private void listView1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            int index = listView1.SelectedItems[0].Index;
            Recipe r = foundRecipes[index];
            AddRecipe ar = new AddRecipe(server, user,false);
            ar.LoadRecipe(r);
            ar.ShowDialog();
        }


    }
}
