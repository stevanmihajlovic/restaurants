﻿using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB_Repository.Domain_models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MongoDB_Repository.Forms
{
    public partial class CreateNewIngredient : Form
    {
        MongoServer server;
        List<Ingredient> ingredients;
        public CreateNewIngredient (MongoServer s, List<Ingredient> ings)
        {
            server = s;
            ingredients = ings;
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBoxName.Equals("") || comboBoxType.SelectedIndex == -1)
                MessageBox.Show("Please fill all the fields!");
            else
            {
                Ingredient ing = new Ingredient();

                ing.Name = textBoxName.Text;
                ing.Type = comboBoxType.SelectedItem.ToString();

                MongoDatabase db = server.GetDatabase("RecipeDatabase");
                MongoCollection collection = db.GetCollection("ingredients");

                IMongoQuery query = Query.EQ("Name", ing.Name);
                MongoCursor<Ingredient> ings = collection.FindAs<Ingredient>(query);
                Ingredient[] list = ings.ToArray<Ingredient>();
                if (list.Length == 0)//check if unique
                {
                    collection.Insert(ing);
                    ingredients.Add(ing);
                    MessageBox.Show("Succesfully added " + ing.Name + " as a new ingredient!");
                    Dispose();
                }
                else
                {
                    MessageBox.Show("Ingredient already exists!");
                }
            }
        }
    }
}
