﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;
using System.Drawing;

namespace MongoDB_Repository.Domain_models
{
    public class Recipe
    {
        public ObjectId id;
        public MongoDBRef owner;
        public string Name;
        public List<MongoDBRef> ingredients;
        public List<string> quantities;
        public string Directions;
        public int TimeToPrepare;       
        public string Type;
        public Image image;
        //public List<MongoDBRef> comments;

        public Recipe ()
        {
            ingredients = new List<MongoDBRef>();
            quantities = new List<string>();
        }

        public void AddRecipe (MongoServer server)
        {
            MongoDatabase database = server.GetDatabase("RecipeDatabase");
            MongoCollection collection = database.GetCollection<Recipe>("recipes");
            collection.Insert(this);            
        }

    }
}
