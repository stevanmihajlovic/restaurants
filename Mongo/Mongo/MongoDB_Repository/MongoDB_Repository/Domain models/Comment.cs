﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MongoDB_Repository.Domain_models
{
    public class Comment
    {
        public ObjectId id;
        public string Title;
        public string Text;
        public int Rating; //1-5?
        public MongoDBRef AttachedTo;
        //public List<MongoDBRef> Comments; //da mogu i prodavnice da se ocenjuju ili ne???

        public Comment()
        {
            
        }
    }
}
