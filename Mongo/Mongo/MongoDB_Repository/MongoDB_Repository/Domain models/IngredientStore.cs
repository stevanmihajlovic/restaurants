﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MongoDB_Repository.Domain_models
{
    public class IngredientStore
    {
        public ObjectId id;
        public string Name; //treba biti unique
        public List<MongoDBRef> Ingredients;
        //public List<MongoDBRef> Comments; //da mogu i prodavnice da se ocenjuju ili ne???

        public IngredientStore()
        {
            Ingredients = new List<MongoDBRef>();
        }
    }
}
