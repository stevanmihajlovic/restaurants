﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MongoDB_Repository.Domain_models
{
    public class Restaurant
    {
        public ObjectId id;
        public string Name; //treba biti unique
        public List<MongoDBRef> Cooks;
        //public List<MongoDBRef> Comments; //da mogu i recepti a i restorani da se ocenjuju?

        public Restaurant()
        {
            Cooks = new List<MongoDBRef>();
        }
    }
}
