﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;
using MongoDB.Bson.Serialization.Attributes;

namespace MongoDB_Repository.Domain_models
{
    public class User
    {

        public ObjectId id;
        public string Username;
        public string Password;
        public string Name;
        public string Lastname;
        public List<MongoDBRef> Receipts;
        //public MongoDBRef Restaurant; //korisnik moze da bude kuvar negde? 

        public User ()
        {
            Receipts = new List<MongoDBRef>();
        }
    }
}
