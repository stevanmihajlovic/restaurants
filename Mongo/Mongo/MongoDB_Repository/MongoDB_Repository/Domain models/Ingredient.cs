﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MongoDB_Repository.Domain_models
{

    public class Ingredient
    {
        public ObjectId id;
        public string Name;
        public string Type;
        public List<MongoDBRef> recipes;

        public Ingredient()
        {
            recipes = new List<MongoDBRef>();
        }

        public void AddReceipt(MongoServer server)
        {
            MongoDatabase database = server.GetDatabase("RecipeDatabase");
            MongoCollection collection = database.GetCollection<Ingredient>("ingredients");
            collection.Insert(this);
        }
    }
}
